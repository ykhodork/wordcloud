This exercise implements a generic service (not amazon specific) that accepts URLs,
fetches the text from the URL and creates a word cloud of the most used words in the text ("for all the URLs you have been given so far").
It is implemented as a Jersey REST service. The available endpoints are as follows:


```
#!java
(async) 
POST http://host:9999/wordcloud?url=

    url - a query param to specify the url to process
```




```
#!java

(sync)
GET http://host:9999/wordcloud?top=
	top (optional) - an integral number to specify a limit on the number of words in the returned word cloud

```


The processing pipeline looks like this:
POST url -> REST endpoint -> buffered kafka queue -> text fetching to HDFS -> filename to kafka queue -> text processing (word counting) -> in memory word cloud (intermittently persisted)

**Addressing the requirements:**

	You can assume that the load of the of the requests is about 1/sec, but make sure that your design can support a load several orders of magnitudes larger than that

The service is a jersey service where the POST operation submits the url into a kafka backed queue (with an in memory buffer queue to increase performance).
There is a text fetching task(s) that dequeues the url and fetches the bulk text from the url, then writes it into an HDFS file, then puts it into a (kafka backed) processing queue.
There is a text processing task(s) that dequeues the file, submits a mapreduce word counting job for the file, and computes the results into a word in-memory cache.

	
	URLs might repeat themselves, and as with any good engineering system, we would like to avoid fetching the same page over and over again

There's an LRU time limited cache to skip processed urls

	The system needs to be durable, restarting the service should go display the word cloud of all the links received up to this point

There's a persister task that saves the current cloud to disk every 10 seconds, and loads it from disk on server restart.

	Assume the word corpus can be very large, so a naive calculation of all the top words in
	the corpus will not be quick enough to render promptly on the page. You will need to
	address it either through the algorithm, data structures or technology

This is an async service, and there's a pagination option on the GET method to get the word cloud

	We would want to avoid having very common words in the word cloud (e.g. a, the, is)
I put some trivial filtering on the type of words (length, dictionary etc.) as part of the mapreduce job

	This is not a front­end development assignment, please don’t spend too much time on
	making the word cloud pretty. It’s ok, if it’s even a textual representation of the data
	powering the word cloud

The method to get the word cloud is a GET endpoint that returns a map of word->count