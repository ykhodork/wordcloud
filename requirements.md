Technical Details:

● You can assume that the load of the of the requests is about 1/sec, but make sure that

your design can support a load several orders of magnitudes larger than that

● The list of URLs will be provided through a set of CURL calls. Please use the attached

simulateRequests.sh​script to simulate those requests while you develop

● URLs might repeat themselves, and as with any good engineering system, we would like

to avoid fetching the same page over and over again

● The system needs to be durable, restarting the service should go display the word cloud

of all the links received up to this point

● Assume the word corpus can be very large, so a naive calculation of all the top words in

the corpus will not be quick enough to render promptly on the page. You will need to

address it either through the algorithm, data structures or technology

● We would want to avoid having very common words in the word cloud (e.g. a, the, is)

● This is not a front­end development assignment, please don’t spend too much time on

making the word cloud pretty. It’s ok, if it’s even a textual representation of the data

powering the word cloud

Assignment Review Details

This assignment is a test of your systems engineering skills more than your coding skills, so

while we would like to see clean and organized code, it is more important that the system is built

with the right components, can scale well, and you will be able to explain every design decision

that you have made building this system.

Frequently Asked Questions

Q: ​How "generic" the crawler should be? Should it be able to find and extract any significant word corpus

from any web page or it can assume to only handle Amazon product pages and lookup for the description

div?

A: ​It can be limited to specifically Amazon product pages, but you might get asked how to make it more

generalizable

Q: ​Should the word cloud be only consisting of single word terms or it can contain tuples? (the example

word cloud image has a tag "following people") If yes, then what the max size of the tuple?

A: ​Single words

Q: ​How should it handle word inflections such as plural or past tense? I.e. "interest", "interests", "interested"

­ can be considered as three different words with count=1 or as a single word "interest" with count=3.

A: ​Stemming would be nice, but is not required

Q: ​Can the system assume English to be only language?

A: ​Yes

Q: ​Should the system be able to serve word cloud per url and/or for all the indexed urls?

A: ​For all the URLs you have been given so far

Q: ​Is it enough to return counts per word or should the system return urls containing the word?

A: ​Just counts

Q: ​What should be order of words in the word cloud: descending by count or just alphabetically?

A: ​In word clouds usually the size corresponds to the count

Q: ​Should the word counts be precise or approximate?

A: ​Since most word clouds are visual, approximate counts will not drastically change the visualization