package com.yevgenius.wordcloud;

import javax.servlet.http.HttpServlet;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.sun.jersey.spi.container.servlet.ServletContainer;


public class WordCloudServer extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7142883597316165524L;
	
	public static void main(String[] args) throws Exception {
        ServletHolder sh = new ServletHolder(ServletContainer.class);    
        sh.setInitParameter("com.sun.jersey.config.property.resourceConfigClass", "com.sun.jersey.api.core.PackagesResourceConfig");
        sh.setInitParameter("com.sun.jersey.config.property.packages", "com.yevgenius.wordcloud.service"); //Set the package where the services reside
        sh.setInitParameter("com.sun.jersey.api.json.POJOMappingFeature", "true");
      
        Server server = new Server(9999);
        ServletContextHandler context = new ServletContextHandler(server, "/", ServletContextHandler.NO_SECURITY);
        context.addServlet(sh, "/*");
        context.start();
        server.start();
//        try {
//        	FileUtils.deleteDirectory(new File("/usr/local/var/lib/kafka-logs"));
//        } catch (IOException e) {
//        	System.out.println("WARNING: Failed to clear kafka logs");
//        }
        server.join();      
     }
	

}
