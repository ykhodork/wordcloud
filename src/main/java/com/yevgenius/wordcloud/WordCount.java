package com.yevgenius.wordcloud;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.RunningJob;
import org.apache.hadoop.mapred.SequenceFileOutputFormat;
import org.apache.hadoop.mapred.TextInputFormat;

public class WordCount {

	private static String getBody(Text text) {
		String line = text.toString();
		String[] chunks = line.split("class=\"productDescriptionWrapper\"");
		if (chunks.length > 1) {
			chunks = chunks[1].split("div");
			if (chunks.length > 0) {
				return chunks[0];
			}

		}
		return line;
	}
	
	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {
		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();

		public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			String line = getBody(value);
			line = line.replaceAll("[^a-zA-Z ]", " ").toLowerCase();;
			StringTokenizer tokenizer = new StringTokenizer(line);
			while (tokenizer.hasMoreTokens()) {
				word.set(tokenizer.nextToken());
				
				// TODO: skip "non words"
				if (word.getLength() < 4 || word.getLength() > 15) {
					continue;
				}
				output.collect(word, one);
			}
		}
	}

	public static class Reduce extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {
		public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output,
				Reporter reporter) throws IOException {
			int sum = 0;
			while (values.hasNext()) {
				sum += values.next().get();
			}
			output.collect(key, new IntWritable(sum));
		}
	}

	private final JobConf conf;

	public boolean run() {
		try {
			RunningJob job = JobClient.runJob(conf);
			if (!job.isSuccessful()) {
				System.out.println(
						logPrefix() + "MAPRED (FAILURE): " + job.getFailureInfo() + " Job name: " + conf.getJobName());
				return false;
			}
			System.out.println(logPrefix() + "MAPRED SUCCESS: " + job.getJobName() + ", at "
					+ FileOutputFormat.getOutputPath(conf));
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public WordCount(Path input, Path output) {

		conf = new JobConf(WordCount.class);
		conf.setJobName(input.getName());

		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(IntWritable.class);

		conf.setMapperClass(Map.class);
		conf.setCombinerClass(Reduce.class);
		conf.setReducerClass(Reduce.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(SequenceFileOutputFormat.class);
		conf.set("mapred.task.timeout", "60000"); // set timeout to 1 minute

		FileInputFormat.setInputPaths(conf, input);
		FileOutputFormat.setOutputPath(conf, output);

	}

	public static String logPrefix() {
		return new Date().toInstant().toString() + " ";
	}

}