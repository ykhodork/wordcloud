package com.yevgenius.wordcloud.service;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

/**
 * A queue backed by Kafka
 * 
 * @author ykhodorkovsky
 *
 */
class MessageQueue implements Closeable {
	private final Producer<String, String> producer;
	private final BlockingQueue<KeyedMessage<String, String>> producerBuffer;
	private final Thread producerWorker;

	public MessageQueue() {

		producerBuffer = new LinkedBlockingQueue<>();
		ProducerConfig config = new ProducerConfig(getProducerProps());
		producer = new Producer<String, String>(config);
		producerWorker = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						producer.send(producerBuffer.take());
					} catch (Exception e) {
						// TODO log warning
						e.printStackTrace();
					}
				}

			}
		}, "producer-worker");
		producerWorker.start();

	}

	public static interface Dequeuer<K, V> extends Closeable {
		ConsumerRecords<K, V> dequeue();
	}

	public Dequeuer<String, String> createDequeuer(final List<String> topics) {
		final KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(getConsumerProps());
		consumer.subscribe(topics);

		return new Dequeuer<String, String>() {

			@Override
			public synchronized ConsumerRecords<String, String> dequeue() {
				return consumer.poll(5000);
			}

			@Override
			public synchronized void close() throws IOException {
				consumer.close();
			}
		};
	}

	/**
	 * Put a value into this Queue
	 * 
	 * @param topic
	 * @param message
	 * @return true iff value was successfully put into queue
	 */
	public boolean insert(String topic, String message) {
		KeyedMessage<String, String> data = new KeyedMessage<>(topic, message);
		if (!producerBuffer.offer(data)) {
			// TODO handle
			return false;
		}
		return true;
	}

	@Override
	public synchronized void close() throws IOException {
		producerWorker.interrupt();
		producer.close();

	}

	private static Properties getConsumerProps() {
		// specify some consumer properties
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "test-consumer-group");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "200");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		return props;

	}

	private static Properties getProducerProps() {
		// init kafka producer
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("metadata.broker.list", "localhost:9092");
		props.put("producer.type", "sync");
		props.put("request.required.acks", "0");
		props.put("serializer.class", "kafka.serializer.StringEncoder");
		props.put("client.id", "test-consumer-group");
		return props;
	}

}
