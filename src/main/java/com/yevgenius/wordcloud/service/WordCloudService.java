/**
 * 
 */
package com.yevgenius.wordcloud.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

import javax.naming.ServiceUnavailableException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Weigher;
import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.sun.jersey.spi.resource.Singleton;
import com.yevgenius.wordcloud.WordCount;
import com.yevgenius.wordcloud.service.MessageQueue.Dequeuer;

/**
 * @author ykhodorkovsky
 *
 */
@Singleton
@javax.ws.rs.Path("/wordcloud")
public class WordCloudService {

	static final String URLS_TOPIC = "urls_topic";
	static final String TEXT_TOPIC = "text_topic";
	static final int NUM_WORKER_THREADS = 4;
	static final String HADOOP_FS_PREFIX = "hdfs://localhost:9000/";

	private static final String FS_HOME = HADOOP_FS_PREFIX + "wordcloud/";
	private static final Path WORD_STORE_FILE = new Path(FS_HOME + "wordcloud.local");

	private static final int LRU_SIZE_BYTES = 1024 * 1024 * 1024; // 1GB
	private static final int CLOUD_MAX_SIZE = 1000;
	public static final Set<String> SKIPWORDS = new HashSet<>(Arrays.asList("class", "amazon", "divtoupdate"));

	private final ConcurrentMap<String, Long> CACHE;
	private final ConcurrentMap<String, Long> WORDCLOUD;
	private final MessageQueue QUEUE;

	public WordCloudService() {
		CACHE = createCache(LRU_SIZE_BYTES);

		WORDCLOUD = createWordcloudMap();
		WORDCLOUD.putAll(loadWordcloud());

		QUEUE = new MessageQueue();
		// Start consumer background workers
		ExecutorService executorService = Executors.newFixedThreadPool(NUM_WORKER_THREADS);
		executorService.submit(new TextFetcherTask(QUEUE.createDequeuer(Arrays.asList(URLS_TOPIC))));
		executorService.submit(new TextProcessorTask(QUEUE.createDequeuer(Arrays.asList(TEXT_TOPIC))));
		executorService.submit(new PersisterTask());
	}

	private synchronized ConcurrentMap<String, Long> loadWordcloud() {
		FileSystem hdfs = getHFSHandle();
		try {
			if (hdfs.exists(WORD_STORE_FILE)) {
				
				FSDataInputStream is = hdfs.open(WORD_STORE_FILE);
				ObjectInputStream ois = new ObjectInputStream(is.getWrappedStream());
				@SuppressWarnings("unchecked")
				ConcurrentMap<String, Long> wordcloud = (ConcurrentMap<String, Long>)ois.readObject();
				ois.close();
				return wordcloud;

			}
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Maps.newConcurrentMap();
	}

	private synchronized void saveWordcloud() {
		FileSystem hdfs = getHFSHandle();
		try {
			FSDataOutputStream os = hdfs.create(WORD_STORE_FILE, true);
					
			ObjectOutputStream oos = new ObjectOutputStream(os.getWrappedStream());
			oos.writeObject(new ConcurrentHashMap<>(WORDCLOUD));
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class PersisterTask implements Runnable {

		@Override
		public void run() {
			while (true) {
				saveWordcloud();
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}
	
	class TextProcessorTask implements Runnable {

		private final Dequeuer<String, String> dequeuer;
		private FileSystem hdfs;

		public TextProcessorTask(Dequeuer<String, String> dequeuer) {
			this.dequeuer = dequeuer;
			this.hdfs = getHFSHandle();
		}

		@Override
		public void run() {
			while (true) {
				ConsumerRecords<String, String> records = dequeuer.dequeue();
				for (ConsumerRecord<String, String> record : records) {
					final String fileName = record.value();
					System.out.println(logPrefix() + " Dequeued file " + fileName);
					final Path output = new Path(FS_HOME + getOutputPath(fileName));
					final Path input = new Path(FS_HOME + fileName);

					try {
						if (hdfs.exists(output)) {
							hdfs.delete(output, true);
							// TODO continue; or handle duplicate name
						}
					} catch (IOException e) {
						e.printStackTrace();
					}

					// If the hadoop job completed successfully, read the
					// results into the words map
					if (new WordCount(input, output).run()) {

						try {
							FileStatus[] fss = hdfs.listStatus(output);
							for (FileStatus outputFile : fss) {
								Path path = outputFile.getPath();

								// skip directories and non "part-*" files
								if (outputFile.isDirectory() || !path.getName().contains("part-"))
									continue;

								SequenceFile.Reader reader = new SequenceFile.Reader(new Configuration(),
										SequenceFile.Reader.file(path));

								Text key = new Text();
								IntWritable value = new IntWritable();
								while (reader.next(key, value)) {

									//skip uninteresting words
									if (skipWord(key.toString())) {
										continue;
									}

									WORDCLOUD.putIfAbsent(key.toString(), 0L);
									WORDCLOUD.compute(key.toString(), new BiFunction<String, Long, Long>() {
										@Override
										public Long apply(String t, Long u) {
											return u += value.get();
										}
									});
								}
								reader.close();
							}
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		}

		private boolean skipWord(String word) {
			return SKIPWORDS.contains(word);
		}

	}

	private static FileSystem getHFSHandle() {
		Configuration configuration = new Configuration();
		try {
			return FileSystem.get(new URI(HADOOP_FS_PREFIX), configuration);
		} catch (IOException | URISyntaxException e) {
			// TODO log error
			e.printStackTrace();
			return null;
		}

	}

	class TextFetcherTask implements Runnable {

		private final Dequeuer<String, String> dequeuer;
		FileSystem hdfs;

		public TextFetcherTask(Dequeuer<String, String> dequeuer) {
			this.dequeuer = dequeuer;
			this.hdfs = getHFSHandle();
		}

		private InputStreamReader getReader(String url) {
			URLConnection connection;
			try {
				connection = new URL(url).openConnection();
				connection.setConnectTimeout(5000);
				connection.setReadTimeout(20000);
				connection.connect();
				return new InputStreamReader(connection.getInputStream());
			} catch (IOException e) {
				// TODO log error
				System.out.println(logPrefix() + e.getMessage());

				return null;
			}

		}

		private OutputStreamWriter getWriter(String fileName) {
			Path file = new Path(FS_HOME + fileName);
			try {
				if (hdfs.exists(file)) {
					// already processed, but cache should catch it
					// TODO: log info
					hdfs.delete(file, true);
				}
			} catch (IOException e) {
				// TODO log warning
				e.printStackTrace();
			}

			OutputStream os;
			try {
				os = hdfs.create(file, false);
			} catch (IOException e) {
				// TODO log error
				return null;
			}
			return new OutputStreamWriter(os);
		}

		@Override
		public void run() {

			while (true) {
				ConsumerRecords<String, String> records = dequeuer.dequeue();
				for (ConsumerRecord<String, String> record : records) {

					final String url = record.value();
					final String fileName = getFileName(url);
					if (CACHE.putIfAbsent(fileName, Thread.currentThread().getId()) != null) {
						// Don't fetch this file as it has been fetched in
						// another thread
						continue;
					}

					try (Reader reader = getReader(url); Writer writer = getWriter(fileName);) {
						if (reader == null || writer == null) { // TODO remove
																// debug
																// statement
							System.out.println(
									logPrefix() + "ERROR: " + (reader == null ? "reader" : "writer") + " is null");
							throw new NullPointerException();
						}
						// Buffered read from url to a file in HDFS
						// http://commons.apache.org/proper/commons-io/javadocs/api-2.4/org/apache/commons/io/IOUtils.html
						long charsRead = IOUtils.copyLarge(reader, writer);
						System.out.println(logPrefix() + " Read text into file=" + fileName + " of length=" + charsRead
								+ ", from url=" + url);
						QUEUE.insert(TEXT_TOPIC, fileName);
					} catch (IOException | NullPointerException e) {
						// TODO log error
						CACHE.remove(fileName, Thread.currentThread().getId()); // Didn't
																				// fetch
																				// successfully
						continue;
					}
				}
			}
		}
	}

	public static String getFileName(String url) {
		return String.valueOf(Math.abs(url.hashCode())) + ".input";
	}

	public static String logPrefix() {
		return new Date().toInstant().toString() + " ";
	}

	/**
	 * @param top
	 *            how many entries of the wordcloud to return
	 * @return json map of the requested wordcloud
	 */
	@GET
	@javax.ws.rs.Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public String getCloud(@QueryParam("top") Integer top) {

		Map<String, Long> wordcloud;
		if (top != null) {
			wordcloud = getTopValuesSubMap(WORDCLOUD, top);
		} else {
			wordcloud = WORDCLOUD;
		}
		Gson gson = new Gson();
		return gson.toJson(wordcloud);
	}

	@POST
	@javax.ws.rs.Path("/")
	public void postPage(@QueryParam("url") String url) throws ServiceUnavailableException {

		// Exists in cache or submit for processing
		if (CACHE.putIfAbsent(url, 1L) == null) {

			// Submit the url for processing
			try {
				url = URLDecoder.decode(url, Charsets.UTF_8.displayName());
			} catch (UnsupportedEncodingException e) {
				throw new javax.ws.rs.WebApplicationException();
			}
			if (!QUEUE.insert(URLS_TOPIC, url)) {
				throw new ServiceUnavailableException("Service Temporarily Unavailable");
			}
		} else {
			System.out.println(logPrefix() + " Cache hit, skip processing. url= " + url);
		}
	}

	/**
	 * Creates an LRU ConcurrentMap limited at 1GB
	 * 
	 * TODO use https://github.com/ben-manes/caffeine
	 */
	private static ConcurrentMap<String, Long> createCache(int maxSizeBytes) {

		Cache<String, Long> cache = Caffeine.newBuilder().weigher(new Weigher<String, Long>() {
			@Override
			public int weigh(String key, Long value) {
				return 4 + key.length() * 2;
			}
		}).maximumWeight(maxSizeBytes).expireAfterWrite(10, TimeUnit.MINUTES).build();
		return cache.asMap();
	}

	private static ConcurrentMap<String, Long> createWordcloudMap() {
		Cache<String, Long> cache = Caffeine.newBuilder().maximumSize(CLOUD_MAX_SIZE).build();
		return cache.asMap();
	}

	private String getOutputPath(String fileName) {
		String[] tokens = fileName.split("[.]");
		return tokens[0] + "-output";
	}

	private static <K, V extends Comparable<? super V>> Map<K, V> getTopValuesSubMap(Map<K, V> map, int topValues) {

		// Some unsubstantiated safeguards :)
		if (topValues < 0) {
			topValues = 0;
		}
		if (topValues > 1000) {
			topValues = 1000;
		}
		

		List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Map<K, V> sortedMap = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		Map<K, V> result = new LinkedHashMap<>(topValues);
		for (Map.Entry<K, V> entry : sortedMap.entrySet()) {
			if (topValues <= 0)
				break;
			result.put(entry.getKey(), entry.getValue());
			topValues -= 1;
		}
		return result;
	}

}
