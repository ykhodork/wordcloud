#!/bin/bash
cd ../target
export JAVA_HOME="$(/usr/libexec/java_home)"
export HADOOP_HOME=/usr/local/Cellar/hadoop/2.7.1/libexec
export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib
export HADOOP_OPTS="$HADOOP_OPTS -Djava.library.path=$HADOOP_HOME/lib"
export HADOOP_CLASSPATH=$HADOOP_HOME/share/hadoop/common
java -server -jar wordcloud-1.0-SNAPSHOT-jar-with-dependencies.jar $1
